using BookingProtoApp;
using Grpc.Core;
using Newtonsoft.Json;
using PaymentService.Server.Models;
using PaymentService.Utils;

namespace BookingServiceApp.Services
{
    public class BookingService : BookingProto.BookingProtoBase
    {

        private readonly IConfiguration _config;

        public BookingService(IConfiguration config)
        {
            _config = config;
        }

        public override async Task<BookingProtoResponse> BookingProtoWeather(BookingProtoRequest request, ServerCallContext context)
        {
            return await Task.FromResult(new BookingProtoResponse { Data = "Testing endpoint - success" });
        }

        public override async Task<BookingProtoOperationHistoryResponse> BookingProtoPaymentOperationHistory(BookingProtoOperationHistoryRequest request, ServerCallContext context)
        {
            var accessToken = _config.GetValue<string>("AppSettingsYoomoney:PermanentToken");

            var operationHistoryRequest = new OperationHistoryRequest();

            if (request.Details != null && request.Details != "")
            {
                operationHistoryRequest.Details = bool.Parse(request.Details);
            }

            if (request.From != null && request.From != "")
            {
                operationHistoryRequest.From = DateTime.Parse(request.From);
            }

            if (request.Label != null && request.Label != "")
            {
                operationHistoryRequest.Label = request.Label.ToString();
            }

            if (request.Records != null && request.Records != 0)
            {
                operationHistoryRequest.Records = (int)request.Records;
            }

            if (request.StartRecord != null && request.StartRecord != "")
            {
                operationHistoryRequest.StartRecord = request.StartRecord;
            }

            if (request.Till != null && request.Till != "")
            {
                operationHistoryRequest.Till = DateTime.Parse(request.Till);
            }

            if (request.Type != null && request.Type != "")
            {
                operationHistoryRequest.Type = request.Type;
            }

            var response = await PaymentUtils.GetOperationHistoryAsync(accessToken, operationHistoryRequest);
            var responseContent = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<OperationHistoryResponse>(responseContent);

            var bookingProtoOperationHistoryResponse = new BookingProtoOperationHistoryResponse();

            foreach (var o in data.Operations)
            {
                var Operation = new BookingProtoOperationHistoryOperation
                {
                    GroupId = o.Group_id,
                    Details = o.Details ?? "",
                    OperationId = o.Operation_id,
                    Title = o.Title,
                    Amount = o.Amount,
                    Direction = o.Direction,
                    Datetime = o.Datetime,
                    Label = o.Label ?? "",
                    Status = o.Status,
                    Type = o.Type,
                    AmountCurrency = o.Amount_currency,
                    IsSbpOperation = o.Is_sbp_operation,
                };

                foreach (var spendingCategory in o.spendingCategories ?? [])
                {
                    Operation.SpendingCategories.Add(new BookingProtoOperationHistorySpendingCategories
                    {
                        Name = spendingCategory.name,
                        Sum = spendingCategory.sum
                    });

                }

                foreach (var availableOperation in o.available_operations ?? [])
                {
                    Operation.AvailableOperations.Add(availableOperation);
                }


                bookingProtoOperationHistoryResponse.Operations.Add(Operation);
            }


            return await Task.FromResult(bookingProtoOperationHistoryResponse);
        }

        public override async Task<BookingProtoOperationDetailsResponse> BookingProtoPaymentOperationDetails(BookingProtoOperationDetailsRequest request, ServerCallContext context)
        {
            var accessToken = _config.GetValue<string>("AppSettingsYoomoney:PermanentToken");

            var response = await PaymentUtils.GetOperationDetailsAsync(accessToken, request.OperationId);
            var responseContent = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<OperationDetailsResponse>(responseContent);

            return await Task.FromResult(new BookingProtoOperationDetailsResponse
            {
                OperationId = data.Operation_id ?? "",
                Status = data.Operation_id ?? "",
                PatternId = data.Pattern_id ?? "",
                Direction = data.Direction ?? "",
                Amount = data.Amount ?? "",
                AmountDue = data.Amount_due ?? "",
                Fee = data.Fee ?? "",
                Datetime = data.Datetime.ToString() ?? "",
                Title = data.Title ?? "",
                Sender = data.Sender ?? "",
                Recipient = data.Recipient ?? "",
                RecipientType = data.Recipient_type ?? "",
                Message = data.Message ?? "",
                Comment = data.Comment ?? "",
                Label = data.Label ?? "",
                Details = data.Details ?? "",
                Type = data.Type ?? "",
                DigitalGoods = JsonConvert.SerializeObject(data.Digital_goods) ?? "",
            });
        }


        public override Task<BookingProtoGeneratePaymentLinkResponse> BookingProtoPaymentGeneratePaymentLink(BookingProtoGeneratePaymentLinkRequest request, ServerCallContext context)
        {
            var paymentLinkRequest = new PaymentLinkRequest();

            if (request.Sum != null && request.Sum != "")
            {
                paymentLinkRequest.Sum = request.Sum;
            }

            if (request.Label != null && request.Label != "")
            {
                paymentLinkRequest.Label = request.Label;
            }

            if (request.SuccessURL != null && request.SuccessURL != "")
            {
                paymentLinkRequest.SuccessURL = request.SuccessURL;
            }

            var paymentLink = PaymentUtils.GetPaymentLink(paymentLinkRequest);

            return Task.FromResult(new BookingProtoGeneratePaymentLinkResponse { Url = paymentLink });
        }
    }
}