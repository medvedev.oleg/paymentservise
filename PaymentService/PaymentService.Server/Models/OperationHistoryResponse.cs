﻿namespace PaymentService.Server.Models
{
    public class OperationHistoryResponse
    {
        public OperationHistoryOperation[] Operations { get; set; }
    }

    public class OperationHistoryOperation
    {
        public string Group_id { get; set; }
        public string Details { get; set; }
        public string Operation_id { get; set; }
        public string Title { get; set; }
        public float Amount { get; set; }
        public string Direction { get; set; }
        public string Datetime { get; set; }
        public string Label { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public OperationHistorySpendingCategories[] spendingCategories { get; set; }
        public string Amount_currency { get; set; }
        public bool Is_sbp_operation { get; set; }
        public string[] available_operations { get; set; }
    }

    public class OperationHistorySpendingCategories
    {
        public string name { get; set; }
        public float sum { get; set; }
    }
} 