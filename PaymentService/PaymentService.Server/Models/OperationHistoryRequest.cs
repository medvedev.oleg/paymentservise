﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PaymentService.Server.Models
{
    public class OperationHistoryRequest
    {
        public string? Type { get; set; }
        public string? Label { get; set; }
        public Nullable<DateTime> From { get; set; }
        public Nullable<DateTime> Till { get; set; }
        public string? StartRecord { get; set; }
        
        [Range(1, 100)]
        public int? Records { get; set; }

        public bool? Details  { get; set; }        
    }
}