﻿
namespace PaymentService.Server.Models
{
    public class PaymentLinkRequest
    {  
        public string Sum { get; set; }
        public string? Label { get; set; }
        public string? SuccessURL { get; set; }
    }
}