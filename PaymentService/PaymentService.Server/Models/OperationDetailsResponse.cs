﻿using System;

namespace PaymentService.Server.Models
{
    public class OperationDetailsResponse
    {
        public string Operation_id { get; set; }
        public string Status { get; set; }
        public string Pattern_id { get; set; }
        public string Direction { get; set; }
        public string Amount { get; set; }
        public string Amount_due { get; set; }
        public string Fee { get; set; }
        public DateTime Datetime { get; set; }
        public string Title { get; set; }
        public string Sender { get; set; }
        public string Recipient { get; set; }
        public string Recipient_type { get; set; }
        public string Message { get; set; }
        public string Comment { get; set; }
        public string Label { get; set; }
        public string Details { get; set; }
        public string Type { get; set; }
        public object Digital_goods { get; set; } 
    }
}