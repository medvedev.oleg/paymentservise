﻿namespace PaymentService.Server.Models
{
    public class PaymentLinkResponse
    {
        public string Url { get; set; }
    }
}