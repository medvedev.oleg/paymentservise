using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PaymentService.Server.Constants;
using PaymentService.Server.Models;
using PaymentService.Utils;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json.Serialization;

namespace PaymentService.Server.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PaymentController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly ILogger<PaymentController> _logger;

        public PaymentController(ILogger<PaymentController> logger, IConfiguration config)
        {
            _config = config;
            _logger = logger;
        }

        [HttpPost("GetOperationHistory")]
        [SwaggerOperation(
          Summary = "����� ��������� ������������� ������� �������� (��������� ��� ��������) �" +
            " ������������ ������. ������ ������� �������� � �������� ��������������� �������: �� ��������� � ����� ������.",
          Description = "https://yoomoney.ru/docs/wallet/user-account/operation-history"
         )]
        public async Task<OperationHistoryResponse> GetOperationHistoryAsync(OperationHistoryRequest request)
        {
            var accessToken = _config.GetValue<string>("AppSettingsYoomoney:PermanentToken");

            var response = await PaymentUtils.GetOperationHistoryAsync(accessToken, request);
              
            var responseContent = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<OperationHistoryResponse>(responseContent);

            switch (response.StatusCode)
            {
                case HttpStatusCode.Accepted:
                case HttpStatusCode.OK:
                    return data;
                default:
                    return null;
            }
        }

        [HttpPost("GetOperationDetails")]
        [SwaggerOperation(
          Summary = "��������� �������� ��������� ���������� �� �������� �� �������",
          Description = "https://yoomoney.ru/docs/wallet/user-account/operation-details"
         )]
        public async Task<ActionResult<OperationDetailsResponse>> GetOperationDetailsAsync(string operation_id)
        {
            var accessToken = _config.GetValue<string>("AppSettingsYoomoney:PermanentToken");

            var response = await PaymentUtils.GetOperationDetailsAsync(accessToken, operation_id);
              
            var responseContent = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<OperationDetailsResponse>(responseContent);

            switch (response.StatusCode)
            {
                case HttpStatusCode.Accepted:
                case HttpStatusCode.OK:
                    return Ok(data);
                default:
                    return new NotFoundResult();
            }
        }

        [HttpPost("GeneratePaymentLink")]
        [SwaggerOperation(
          Summary = "������������� ������ �� ������",
          Description = "https://yoomoney.ru/docs/payment-buttons/using-api/forms"
        )]
        public PaymentLinkResponse GeneratePaymentLink(PaymentLinkRequest request)
        {
            var paymentLink = PaymentUtils.GetPaymentLink(request);
            return new PaymentLinkResponse { Url = paymentLink };
        }
    }
}
