using PaymentService.Server.Constants;
using PaymentService.Server.Models;

namespace PaymentService.Utils
{
    public class PaymentUtils
    {

        public async static Task<HttpResponseMessage> GetOperationHistoryAsync(string accessToken, OperationHistoryRequest request)
        {

            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {accessToken}");

            var payload = new Dictionary<string, string>();
            // deposition � ���������� �����(������); payment � ������� �� �����(������).
            // ���� �������� �����������, ��������� ��� ��������.
            if (request.Type != null) payload.Add("type", request.Type);

            //����� �������� �� �������� �����. ���������� �������, � ������� ������� �������� �������� ��������� label ������ request-payment.
            if (request.Label != null) payload.Add("label", request.Label);

            //������� �������� �� ������� ������� (��������, ������ from, ��� ����� �������). ���� �������� �����������, ��������� ��� ��������.
            if (request.From != null) payload.Add("from", request.From.Value.ToString("yyyy-MM-ddTHH:mm:ss"));

            //������� �������� �� ������� ������� (�������� ����� ������, ��� till). ���� �������� �����������, ��������� ��� ��������.
            if (request.Till != null) payload.Add("till", request.Till.Value.ToString("yyyy-MM-ddTHH:mm:ss"));

            //���� �������� ������������, �� ����� ���������� ��������, ������� � ������ start_record. �������� ���������� � 0.
            if (request.StartRecord != null) payload.Add("start_record", request.StartRecord);

            //���������� ������������� ������� ������� ��������. ���������� ��������: �� 1 �� 100, �� ��������� � 30.
            if (request.Records != null) payload.Add("records", request.Records.ToString());

            //���������� ��������� ������ ��������. �� ��������� false. ��� ����������� ������� �������� ��������� ������� ����� 
            if (request.Details != null) payload.Add("details", request.Details.ToString().ToLower());

            payload.Add("Content-Type", "application/x-www-form-urlencoded");


            // string jsonContent = JsonConvert.SerializeObject(payload);
            // var content = new StringContent(jsonContent, Encoding.UTF8, "application/x-www-form-urlencoded");

            var response = await httpClient.PostAsync(Yoomoney.UrlOperationHistory, new FormUrlEncodedContent(payload));
            httpClient.Dispose();

            return response;
        }

        public async static Task<HttpResponseMessage> GetOperationDetailsAsync(string accessToken, string operation_id)
        {

            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {accessToken}");

            var payload = new Dictionary<string, string>();
            payload.Add("Content-Type", "application/x-www-form-urlencoded");
            payload.Add("operation_id", operation_id);

            var response = await httpClient.PostAsync(Yoomoney.UrlOperationDetails, new FormUrlEncodedContent(payload));
            httpClient.Dispose();

            return response;
        }


        public static string GetPaymentLink(PaymentLinkRequest request)
        {
            //����� �������� �Money, �� ������� ����� ��������� ������ ������������.
            string receiver = $"?receiver={AccountOptions.Receiver}";

            //��� �����. ������������� �������� � button        
            string quickpayForm = $"&quickpay-form={AccountOptions.QuickpayForm}";

            //������ ������. ��������� ��������:
            // - PC � ������ �� �������� �Money;
            // - AC � � ���������� �����.
            string paymentType = $"&paymentType={AccountOptions.PaymentType}";

            //����������� ����� - 2�. ����� �������� (�������� � �����������).
            string sum = $"&sum={request.Sum}";

            //�����, ������� ���� ��� ���������� ����������� ����������� ��������. ��������, � �������� ����� ����� ��������� ��� ��� ������������� ������.
            string label = request.Label != null ? $"&label={request.Label}" : null;

            //URL-����� ��� ��������� ����� ���������� ��������.
            string successURL = request.SuccessURL != null ? $"&successURL={request.SuccessURL}" : null;

            string paymentLink = String.Concat($"{Yoomoney.UrlQuickpay}/confirm", receiver, quickpayForm, paymentType, label, sum, successURL);


            return paymentLink;
        }

    }
}
