﻿namespace PaymentService.Server.Constants
{
    public class AccountOptions
    {
        public static readonly string Receiver = "4100118552793944";
        public static readonly string QuickpayForm = "button";
        public static readonly string PaymentType = "AC";
    }
}