﻿namespace PaymentService.Server.Constants
{
    public class Yoomoney
    {
        public static readonly string Url = "https://yoomoney.ru";
        public static readonly string UrlOperationDetails = "https://yoomoney.ru/api/operation-details";
        public static readonly string UrlOperationHistory = "https://yoomoney.ru/api/operation-history";
        public static readonly string UrlQuickpay = "https://yoomoney.ru/quickpay";
        public static readonly string QuickpayDefaultLabel = "";
        public static readonly string QuickpayDefaultSuccessURL = "";
    }
}
 