import axios from 'axios';
import { useRef, useState } from 'react';

// Yoomoney документация по авторизации
// https://yoomoney.ru/docs/wallet/using-api/authorization/basics

// Пошаговый гайд, сторонний сайт: Авторизация и получение токена
// https://www.blast.hk/threads/94544/

// Пошаговый гайд, сторонний сайт: Авторизация и получение токена + пример формы оплаты
// https://habr.com/ru/articles/772628/#habracut

const YOOMONEY_URL_AUTHORIZE = "https://yoomoney.ru/oauth/authorize";
const YOOMONEY_URL_TOKEN = "https://yoomoney.ru/oauth/token";
const YOOMONEY_URL_ACCOUNT_INFO = "https://yoomoney.ru/api/account-info";
const YOOMONEY_URL_OPERATION_HISTORY = "https://yoomoney.ru/api/operation-history";

const CLIENT_ID = "382B300578D37AFC415F1A5548A2E936A24D68C9853356D16A004BD6FDC23081";
 
export const AuthorizationForm = () => {
    const refPermanentToken = useRef<HTMLInputElement>(null);
    const refTemporaryToken = useRef<HTMLInputElement>(null);

    const [yoomoneyPaymentLink, setYoomoneyPaymentLink] = useState("");
    const [yoomoneyAauthorizeForm, setYoomoneyAauthorizeForm] = useState<{ __html: string | TrustedHTML } | undefined>({ __html: "" });

    const handlerOnGetAuthorize = async () => {

        axios.post(
            YOOMONEY_URL_AUTHORIZE,
            {
                client_id: CLIENT_ID,
                response_type: "code",
                redirect_uri: "https://t.me/medvedevOV/successfull",
                scope: "account-info operation-history operation-details"
            },
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(function (response) {
                // eslint-disable-next-line no-debugger
                //  debugger;
                //  console.log("response", response);
                setYoomoneyAauthorizeForm({ __html: response.data });
                return response

            })
            .catch(function (error) {
                alert(`error: ${error}`);
            });
    }


    const handlerOnGetPermanentToken = async () => {
        if (refTemporaryToken.current === null) return;

        const Temporary_TOKEN = refTemporaryToken.current?.value;

        if (!Temporary_TOKEN) {
            alert(`Specify the "Temporary token"`);
            return;
        }

        axios.post(
            YOOMONEY_URL_TOKEN,
            {
                code: Temporary_TOKEN,
                client_id: CLIENT_ID,
                grant_type: "authorization_code",
                redirect_uri: "",

            },
            {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            })
            .then(function (response) {
                // eslint-disable-next-line no-debugger
                debugger;
                console.log("response", response);

                return response

            })
            .catch(function (error) {
                alert(`error: ${error}`);
            });
    }

    const handlerOnGetDetails = async () => {
        if (refPermanentToken.current === null) return;

        const PERMANENT_TOKEN = refPermanentToken.current?.value;

        if (!PERMANENT_TOKEN) {
            alert(`Specify the "Permanent token"`);
            return;
        }

        axios.post(
            YOOMONEY_URL_ACCOUNT_INFO,
            {},
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': 'Bearer ' + PERMANENT_TOKEN
                }
            })
            .then(function (response) {
                // eslint-disable-next-line no-debugger
                debugger;
                console.log("response", response);

                return response

            })
            .catch(function (error) {
                alert(`error: ${error}`);
            });
    }

    const handlerOnGetHistory = async () => {
        if (refPermanentToken.current === null) return;

        const PERMANENT_TOKEN = refPermanentToken.current?.value;

        if (!PERMANENT_TOKEN) {
            alert(`Specify the "Permanent token"`);
            return;
        }

        axios.post(
            YOOMONEY_URL_OPERATION_HISTORY,
            {},
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': 'Bearer ' + PERMANENT_TOKEN
                }
            })
            .then(function (response) {
                // eslint-disable-next-line no-debugger
                // debugger;
                console.log("response", response);

                return response

            })
            .catch(function (error) {
                alert(`error: ${error}`);
            });
    }

    const handlerOnGetPaymentLink = async () => {
        //Детальная информация по генерации ссылки
        //https://yoomoney.ru/docs/payment-buttons/using-api/forms
        const REDIRECT_SUCCESS_URL = "http://excursiohub.ru/payment/success";
        const ORDER_ID = 12345; //Мок id заказа

        const receiver = "4100118552793944"; // Номер кошелька ЮMoney, на который нужно зачислять деньги отправителей.
        const quickpayForm = "button"; // Тип формы. Фиксированное значение — button.
        const paymentType = "AC"; // Способ оплаты. Возможные значения: PC — оплата из кошелька ЮMoney; AC — с банковской карты.
        const sum = 2; // Минимальная сумма - 2р. Сумма перевода (спишется с отправителя).
        const label = ORDER_ID; // Метка, которую сайт или приложение присваивает конкретному переводу. Например, в качестве метки можно указывать код или идентификатор заказа.
        const successURL = REDIRECT_SUCCESS_URL;

        const paymentLink = `https://yoomoney.ru/quickpay/confirm?receiver=${receiver}&quickpay-form=${quickpayForm}&paymentType=${paymentType}&sum=${sum}&label=${label}&successURL=${successURL}`
        setYoomoneyPaymentLink(paymentLink);
    }

    return <>
        <div style={{ display: "flex", gap: "1rem" }}>
            <input placeholder='Permanent token' ref={refPermanentToken} className='auth-input' />
            <input placeholder='Temporary token' ref={refTemporaryToken} className='auth-input' />
        </div>
        <p>
            <button type='button' onClick={handlerOnGetAuthorize}>Get Authorize</button>
            <button type='button' onClick={handlerOnGetPermanentToken}>Get permanent token</button>
            <button type='button' onClick={handlerOnGetDetails}>Get details info</button>
            <button type='button' onClick={handlerOnGetHistory}>Get operation-history</button>
            <button type='button' onClick={handlerOnGetPaymentLink}>Get payment link</button>
        </p>

        {yoomoneyPaymentLink && <div><a href={yoomoneyPaymentLink} target='_blank' id="yoomoney-payment-link">Yoomoney Payment Link - Quickpay</a></div>}
        {yoomoneyAauthorizeForm && <div id="yoomoney-authorize" dangerouslySetInnerHTML={yoomoneyAauthorizeForm}></div>}
    </>

}
